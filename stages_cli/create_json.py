import json


class CreateTimer:
    def __init__(self, hours: int, minutes: int, seconds: int):
        """Provides a json in form of an timer

        Use get() to receive the timer as dict.

        Args:
            hours (int): The hours
            minutes (int): The minutes
            seconds (int): The seconds

        """
        self._hours = hours
        self._minutes = minutes
        self._seconds = seconds

    def get(self) -> dict:
        """Return timer representation as dict"""
        timer = {
            "type": "timer",
            "hours": self._hours,
            "minutes": self._minutes,
            "seconds": self._seconds,
        }

        return timer


class CreateStage:
    def __init__(self, name: str, timer: dict):
        self._name = name
        self._timer = timer

    def get(self) -> dict:
        """Return the stage representation as dict"""
        stage = {"type": "stage", "name": self._name, "subjects": []}

        for timer in self._timer:
            stage["subjects"].append(timer)

        return stage


class CreateQueue:
    def __init__(self, name: str, stages: dict):
        self._name = name
        self._stages = stages

    def get(self) -> dict:
        """Return the queue representation as dict"""

        queue = {"type": "stage_queue", "name": "Test queue", "stages": []}

        for stage in self._stages:
            queue["stages"].append(stage)

        return queue

import json
import logging
import threading
import time

import questionary
from stages.stage_queue_builder import StageQueueBuilder

from stages_cli.create_json import CreateQueue, CreateStage, CreateTimer

logging.basicConfig(level=logging.CRITICAL)


def menue_stage_queue():
    queue = None

    answer = questionary.select(
        "What do you want to do?",
        choices=["Create new stage queue", "Start timer", "End"],
    ).ask()

    while answer != "End":

        if answer == "Create new stage queue":

            queue_name = questionary.text(
                "What do you want to call the complete queue?",
                default="Test queue",
            ).ask()

            queue = CreateQueue(name=queue_name, stages=menue_stage()).get()
            queue = StageQueueBuilder().parse_json(json.dumps(queue))

        if answer == "Start timer":
            x = threading.Thread(target=queue.start, args=(), daemon=True)
            x.start()
            old = "old"
            new = "new"

            while x.is_alive():
                new = queue.status
                if new != old:
                    print(queue.status)
                    old = queue.status

        answer = questionary.select(
            "What do you want to do?",
            choices=["Create new stage queue", "Start timer", "End"],
        ).ask()


def menue_stage():

    stages = []

    answer = questionary.select(
        "What do you want to do?", choices=["Add stage", "End"]
    ).ask()

    while answer != "End":

        stage_name = questionary.text(
            "What do you want to call this stage?",
            default="Test stage",
        ).ask()

        stages.append(CreateStage(name=stage_name, timer=menue_timer()).get())

        answer = questionary.select(
            "What do you want to do?", choices=["Add stage", "End"]
        ).ask()

    return stages


def menue_timer():
    timer = []

    answer = questionary.select(
        "What do you want to do?", choices=["Add timer", "End"]
    ).ask()

    while answer != "End":

        hours = int(
            questionary.text(
                "How many hours?",
                default="0",
            ).ask()
        )

        minutes = int(
            questionary.text(
                "How many minutes?",
                default="0",
            ).ask()
        )

        seconds = int(
            questionary.text(
                "How many seconds?",
                default="1",
            ).ask()
        )

        timer.append(CreateTimer(hours=hours, minutes=minutes, seconds=seconds).get())

        answer = questionary.select(
            "What do you want to do?", choices=["Add timer", "End"]
        ).ask()

    return timer


if __name__ == "__main__":
    menue_stage_queue()

# Python project template

## Overview

This project templates is a simple python project start, which should make a new project easier. Instead of getting everything to run like the projects before, you could use this template to take care of that.

Within this template you have:

- `pre-commit` for using a `leight weight pipeline` locally
- `sphinx` to generate a documentation (static html) based on the `README.md`
- `requirements.txt` & `requirements_dev.txt` for an easier `venv` installation
- A `GitLab pipeline`, using `sphinx` for the documentation and `GitLab pages`
- A project folder structure
    > Tipp: If you change the folder structure, also change the symlinks linking to `README.md` within `sphinx/`
- Necessary files like `REAADME.md`, `LICENCSE`, `setup.py`,etc.

## Set up

The project also contains a `Makefile` which will set up the complete project. It will also generate a `python virtual environment - called venv` and install the dependencies from `requirements_dev.txt`.

To start the set up type within the root path of your project (where the `Makefile` is located):

```console
grenait@grenait-ThinkCentre:~/GitLab/template$ make
python3 -m venv venv
venv/bin/pip install -U pip
Requirement already satisfied: pip in ./venv/lib/python3.8/site-packages (22.0.4)
venv/bin/pip install -r requirements_dev.txt
Requirement already satisfied: black>=22.1.0 in
...
...
```

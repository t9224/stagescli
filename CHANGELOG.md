# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.0 (2022-04-11)

### Feat

- **cli.py**: adding stages capapility and now mvp of cli
- **cli.py**: added timer and start for the timer
- **all**: CLI for creating the timer jsons
- **cli.py**: first menue for stages

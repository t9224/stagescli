import pytest
import json

from stages_cli.create_json import CreateTimer
from stages_cli.create_json import CreateStage
from stages_cli.create_json import CreateQueue


@pytest.fixture
def timer_string():
    return str(
        json.loads("""{"type": "timer", "hours": 0, "minutes": 0, "seconds": 0}""")
    )


@pytest.fixture
def stage_string():
    return str(
        json.loads(
            """{"type": "stage", "name": "Test stage",
                        "subjects": [{"type": "timer", "hours": 0, "minutes": 0, "seconds": 0},
                        {"type": "timer", "hours": 0, "minutes":0, "seconds": 0}]}
                        """
        )
    )


@pytest.fixture
def queue_string(stage_string):
    queue = """{
        "type":"stage_queue","name":"Test queue","stages":[
            {"type": "stage", "name": "Test stage","subjects": [
                {"type": "timer", "hours": 0, "minutes": 0, "seconds": 0},
                {"type": "timer", "hours": 0, "minutes": 0, "seconds": 0}
                ]
            },
            {"type": "stage", "name": "Test stage","subjects": [
                {"type": "timer", "hours": 0, "minutes": 0, "seconds": 0},
                {"type": "timer", "hours": 0, "minutes": 0, "seconds": 0}
                ]
            }
        ]}"""
    return str(json.loads(queue))


@pytest.fixture
def test_timer():
    return CreateTimer(0, 0, 0).get()


@pytest.fixture
def test_stage(test_timer):
    return CreateStage(name="Test stage", timer=[test_timer, test_timer]).get()


def test_create_timer(timer_string):
    timer = CreateTimer(hours=0, minutes=0, seconds=0)
    assert str(timer.get()) == timer_string


def test_create_stage(test_timer, stage_string):
    stage = CreateStage(name="Test stage", timer=[test_timer, test_timer])
    assert str(stage.get()) == stage_string


def test_create_queue(queue_string, test_stage):
    queue = CreateQueue(name="Test queue", stages=[test_stage, test_stage])
    assert str(queue.get()) == queue_string
